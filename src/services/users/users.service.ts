import {
    BadRequestException,
    Delete,
    Get,
    HttpException,
    HttpStatus,
    Injectable,
    Param,
    Post,
    Put
} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {User, UserDocument} from "../../schemas/user";
import {UserDto} from "../../dto/user-dto";
import {JwtService} from "@nestjs/jwt";
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UsersService {

    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>,
                private jwtService: JwtService) {
    }

    async getAllUsers(): Promise<User[]> {
        return this.userModel.find();
    }

    async getUserById(id): Promise<User> {
        return this.userModel.findById(id);
    }

    async sendUser(data): Promise<User> {
        const userData = new this.userModel(data);
        return userData.save();
    }

    async updateUsers(id: string, body): Promise<User> {
        return this.userModel.findByIdAndUpdate(id, body);
    }

    async deleteUsers(): Promise<User> {
        return this.userModel.remove()
    }

    async deleteUserById(id: string): Promise<User> {
        return this.userModel.findByIdAndRemove(id);
    }

    async checkAuthUser(login: string, psw: string): Promise<User[]> {
        const usersArr = await this.userModel.find({login: login, psw: psw});
        return usersArr.length===0 ? null: usersArr;
    }

    async checkRegUser(login: string): Promise<User[]> {
        return this.userModel.find({login: login});
    }

    async login(user: UserDto) {

        const userFromDb = await this.userModel.find({login: user.login});
        const isPswMatched = bcrypt.compareSync(user.psw, userFromDb[0].psw)
        if (isPswMatched) {
        const payload = { login: user.login, psw: user.psw};
            return {
                id:          userFromDb[0]._id,
                firstName:   userFromDb[0].firstName,
                lastName:    userFromDb[0].lastName,
                phoneNumber: userFromDb[0].phoneNumber,
                email:       userFromDb[0].email,
                cardNumber:  userFromDb[0].cardNumber,
                isAdmin:     userFromDb[0].isAdmin,
                access_token: this.jwtService.sign(payload,{ expiresIn: '24h' })
            };
        } else {
            throw new HttpException('Не найден логин или пароль', HttpStatus.FORBIDDEN);
        }
    }
}
