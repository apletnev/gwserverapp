
export interface IUser {
    login: string,
    firstName?: string
    lastName?: string
    phoneNumber?: string
    email?: string,
    psw: string,
    cardNumber?: string,
    newPsw?: string,
    rptNewPsw?: string,
    id: string,
    isAdmin: boolean
}
