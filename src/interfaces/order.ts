export interface IOrder {
    firstName:   string,
    lastName:    string,
    email:       string,
    phoneNumber: string,
    cardNumber:  string,
    tourId: string,
    userId: string,
    tourName: string,
    _id?:   string
}