export interface ITour {
    name:string,
    description:string,
    type: string,
    img:string,
    id:string
}

export interface ITourClient {
    name:string,
    description:string,
    type:string,
    img:string,
}