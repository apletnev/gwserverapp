import {IUser} from "../interfaces/user";

export class UserDto implements IUser {
    login: string
    psw: string
    firstName: string
    lastName: string
    phoneNumber: string
    email: string
    cardNumber: string
    id: string
    isAdmin: boolean
}

