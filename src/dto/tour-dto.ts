import { ITour } from "src/interfaces/tour"

export class TourDto implements ITour {

    name:string;
    description:string;
    type: string;
    img:string;
    id:string;

    constructor(name, description, type, img) {

        this.name = name;
        this.description = description;
        this.type = type;
        this.img = img;

    }

}
