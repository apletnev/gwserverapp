import { IOrder } from "src/interfaces/order";


export class OrderDto implements IOrder{

    firstName:   string;
    lastName:    string;
    email:       string;
    phoneNumber: string;
    cardNumber:  string;
    tourId: string;
    userId: string;
    tourName: string;
    //_id сделали необязательным
    //_id: string;

    constructor(firstName, lastName, email, phoneNumber, cardNumber, tourId, userId, tourName) {
        this.firstName   = firstName;
        this.lastName    = lastName;
        this.email       = email;
        this.phoneNumber = phoneNumber;
        this.cardNumber  = cardNumber;
        this.tourId = tourId;
        this.userId = userId;
        this.tourName = tourName;
        //this._id = _id;
    }
}