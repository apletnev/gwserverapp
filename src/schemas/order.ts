import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import {IOrder} from "../interfaces/order";

export type OrderDocument = HydratedDocument<Order>;

@Schema()
export class Order implements IOrder{

    @Prop() firstName:   string;
    @Prop() lastName:    string;
    @Prop() email:       string;
    @Prop() phoneNumber: string;
    @Prop() cardNumber:  string;
    @Prop() tourId: string;
    @Prop() userId: string;
    @Prop() tourName: string;

}

export const OrderSchema = SchemaFactory.createForClass(Order);

