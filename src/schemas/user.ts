import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import {IUser} from "../interfaces/user";

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User implements IUser{

    @Prop() login: string;
    @Prop() psw: string;
    @Prop() phoneNumber: string;
    @Prop() email: string;
    @Prop() firstName: string;
    @Prop() lastName: string;
    @Prop() cardNumber: string;
    @Prop() id: string;
    @Prop() isAdmin: boolean;
}

export const UserSchema = SchemaFactory.createForClass(User);
