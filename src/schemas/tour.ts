import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import {ITour} from "../interfaces/tour";

export type TourDocument = HydratedDocument<Tour>;

@Schema()
export class Tour implements ITour{
    
    @Prop() name:string;
    @Prop() description:string;
    @Prop() type: string;
    @Prop() img:string;
    @Prop() id:string;

}

export const TourSchema = SchemaFactory.createForClass(Tour);