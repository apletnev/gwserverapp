import {Body, Controller, Delete, Get, Param, Post} from '@nestjs/common';
import {OrderService} from "../../services/order/order.service";
import {OrderDto} from "../../dto/order-dto";
import {IOrder} from "../../interfaces/order";
import {Order} from "../../schemas/order";

@Controller('order')
export class OrderController {

    constructor(private orderService: OrderService) {}

    @Post()
    initTours(@Body() data: OrderDto): void {
        const orderData = new OrderDto(data.firstName,
                                    data.lastName,
                                    data.email,
                                    data.phoneNumber,
                                    data.cardNumber,
                                    data.tourId,
                                    data.userId,
                                    data.tourName);
        this.orderService.sendOrder(orderData);
    }

    @Get(":userId")
    getOrdersByUserId(@Param ("userId") userId): Promise <IOrder[]> {
        return this.orderService.getOrdersByUserId(userId);
    }

    @Get()
    getAllOrders(): Promise <IOrder[]> {
        return this.orderService.getAllOrders();
    }

    @Delete(":id")
    deleteOrderByOrderId(@Param('id') id): Promise<Order> {
        return this.orderService.deleteOrderByOrderId(id);
    }

}
