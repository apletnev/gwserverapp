import { Module } from '@nestjs/common';
import {AppController} from "../../app.controller";
import {UsersController} from "./users.controller";
import {AppService} from "../../app.service";
import {UsersService} from "../../services/users/users.service";
import {MongooseModule} from "@nestjs/mongoose";
import {User, UserSchema} from "../../schemas/user";
import {AuthService} from "../../services/authentication/auth/auth.service";
import {JwtStrategyService} from "../../services/authentication/jwt-strategy/jwt-strategy.service";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "../../static/private/constants";

@Module({
    imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
              PassportModule,
              JwtModule.register({
                  secret: jwtConstants.secret,
                  //signOptions: { expiresIn: '60s' }
              })],
    controllers: [UsersController],
    providers: [UsersService, AuthService, JwtStrategyService],
})
export class UsersModule {}
