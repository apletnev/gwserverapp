import { Module } from '@nestjs/common';
import { ToursController } from './tours.controller';
import {jwtConstants} from "../../static/private/constants";
import {PassportModule} from "@nestjs/passport";
import {JwtStrategyService} from "../../services/authentication/jwt-strategy/jwt-strategy.service";
import {MongooseModule} from "@nestjs/mongoose";
import {Tour, TourSchema} from "../../schemas/tour";
import {JwtModule} from "@nestjs/jwt";
import {ToursService} from "../../services/tours/tours.service";

@Module({
  controllers: [ToursController],
  imports:  [MongooseModule.forFeature([{ name: Tour.name, schema: TourSchema }]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
    })],
  providers: [ ToursService, JwtStrategyService],
})
export class ToursModule {}
