import { Controller,Delete,Get, Param, Post, UseGuards } from '@nestjs/common';
import { ToursService } from 'src/services/tours/tours.service';
import { JwtAuthGuard } from 'src/services/authentication/jwt-auth.guard/jwt-auth.guard.service';
import { ITour } from 'src/interfaces/tour';
import * as fs from "fs";
import * as path from "path";

@Controller('tours')
export class ToursController {

    constructor(private toursService: ToursService) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    getAllTours(): Promise <ITour[]> {
        return this.toursService.getAllTours();
    }

    @UseGuards(JwtAuthGuard)
    @Get(":id")
    getTourById(@Param ("id")id): Promise <ITour> {
        return this.toursService.getTourById(id);
    }

    @UseGuards(JwtAuthGuard)
    @Delete(":id")
    removeTourById(@Param ("id")id): Promise <[]> {

        //удалим картинку-файл
        this.toursService.getTourById(id).then((tour)=>{
            let filenamePath = "./public/" + tour.img;
            fs.unlink(filenamePath, (err) => {});
        })

        return this.toursService.removeTourById(id);

    }

    @UseGuards(JwtAuthGuard)
    @Delete()
    removeAllTours(): Promise<[]> {

        //удалим файлы
        let directory = "./public/";
        fs.readdir(directory, (err, files) => {
            if (!err){
                files.forEach(file => {
                    if (path.extname(file) == ".jpeg"){
                        fs.unlink(directory + file, (err) => {});
                    }
                })
            }
        });

        return this.toursService.deleteTours();
    }
}
